export function sortBy(arr, p) {
  return arr.sort((a, b) => ((a[p] > b[p]) ? 1 : (a[p] < b[p]) ? -1 : 0));
}

export function cloneDeep(obj) {
  return JSON.parse(JSON.stringify(obj));
}
