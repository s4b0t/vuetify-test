import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';

Vue.use(Vuex);

// initial state
const state = {
  groups: {},
  selected: {},
  loading: false,
  error: false,
};

// getters
const getters = {};

// actions
const actions = {
  fetchData({ commit }) {
    commit('setLoading', true);
    axios({
      timeout: 180000,
    })
      .get('/data.json')
      .then(({ data }) => {
        commit('setGroups', data);
      })
      .catch((error) => {
        const message = error.response.data.message || error.message;
        commit('setError', message);
      })
      .finally(() => commit('setLoading', false));
  },
  setSelected({ commit }, payload) {
    commit('setSelected', payload);
  },
};

// mutations
const mutations = {
  setGroups(state, v) {
    state.groups = v;
  },
  setSelected(state, v) {
    state.selected = v;
  },
  setLoading(state, loading) {
    state.loading = loading;
  },
  setError(state, error) {
    state.error = error;
  },
};

const debug = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({
  state,
  getters,
  actions,
  mutations,
  strict: debug,
});
